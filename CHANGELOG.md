# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to a versioning scheme similar to Matlab, with versions based on the release year.

## [Unreleased]

## [2024a] - 2024-09-26

**Important:**

- The network communication of VA (VANet) was changed, see blow
  - No backward compatibility: e.g. VAMatlab v2023b won't work with VAServer v2024a
  - Hopefully, this will be the last time we need to do breaking changes the network communication as gRPC is upwards compatible

### Added

#### RoomAcousticRenderer

- Now supports adding directivities.

#### SoundPathBasedRenderers

- Allow changing default reflection factor in ini-file
- Enhancement of gain fading
   - Now does a cos-squared instead of linear fading (see `StatefulSmoothedGain` in [ITADSP](https://git.rwth-aachen.de/ita/ITADSP))
   - now also work with Ambisonics spatialization

### Changed

#### VANet

- Cleanup and downsizing of the network interface

#### VAPython

- Full rework to a pure Python package (no C++ bindings anymore), see CHANGELOG.md in VAPython for more details.

### Fixed

#### General

- Minor bugfixes and improvements

## [v2023b] - 2023-11-16

### Added

#### General

- Now default paths (`data` and `conf`) are automatically added and do not need to be added to `VACore.ini`
- The `data` folder now contains additional signals and directivities (e.g. for car pass-by and aircraft noise).

#### AirtrafficNoiseRenderer

- Now allows real-time auralization with inhomogeneous medium
- Data histories for simulation results can now be configured to select interpolation method

#### VAMatlab

- Significant rework/extension of examples: Now most examples
  - ...come with a VACore.ini file
  - ...automatically start the VAServer for you
  - ...have detailed comments and cross-references

### Fixed

#### General

- Fixed several bugs regarding the calculation of air attenuation: in some cases attenuation values could be significantly off
- The `lock_scene()` and `unlock_scene()` did not work anymore due to change of VANet in v2023a, which is fixed now

#### VAServer

- Now properly cleans up the core if closing the window
- Fixed start argument for remote control in `run_VAServer_recording.bat`

### Misc / Develop

- Added packaging of individual projects

## [v2023a] - 2023-06-21

### Changed

**Important:**

- The network communication of VA (VANet) was reworked
  - No backward compatibility: e.g. VAMatlab v2022a won't work with VAServer v2023a

#### General

- New thread-safe logging system with improved formatting and file log
- VAServer: Improved command line interface, with backwards compatibility
- Now all coord transformations are unified throughout VAServer and bindings

#### Rendering

- Soundpath-based renderers now use one SIMO VDL per source instead of one VDL per sound path
- AirTrafficNoiseRenderer now allows scheduling of ART simulations for real-time purposes
- New OutdoorNoiseRenderer which allows binaural, ambisonics and VBAP signals (replaces BinauralOutdoorNoiseRenderer)

#### VAMatlab

- New setup function to add relevant folders to Matlab search path
- Now has a set of convenience classes to render dynamic scenarios
- Supports multiple tracked sources

#### VAPython

- Now uses the Stable ABI
- Thus, can build a single, version independent wheel for Python >= 3.8

### Fixed

- Fixed a bug where third-octave band magnitude spectra were not parsed correctly when calling `SetParameters()`
- VAPython
  - build for msvc22
  - Function argument parsing
  - Fix `Fatal Python error: none_dealloc: deallocating None`

### Misc

- `PiecewisePolynomial` speed-up (ITABase)
- `IIRFilterGenerator::Burg` Now throws proper exception instead of taking sqrt( < 0 ) (ITADSP)
- `VDL` new config class (ITADSP)
- `ART` speed-up for arbitrary weather profiles using pre-calculated data instead of interpolating everytime (ITAGA)

-----------
## [v2022a] - 2022-08-15

### Changed

- Renderer system rework
  - Now distinguishes between FIR-based and sound-path-based renderers
  - Introducing renderer base class and sub-base classes for sound path and FIR renderers
  - Sound-path-based renderers are not restricted to binaural output anymore but can choose from binaural / Ambisonics / VBAP
  - New (renamed) renderers: FreeField, AirTrafficNoise, RoomAcoustics
  - Deprecated or removed renderers: AmbisonicsFreeField, VBAPFreeField, BinauralAirTrafficNoise, BinauralRoomAcoustics
- VAMatlab
  - Now uses IHTATracking, adds option to use AR-Tracking system
- Build system update
  - Exchanged VistaCMakeCommon through new CMake features
  - Update to C++17 standard
  - Now most external libs are automatically downloaded (CPM package management)

### Fixed

- Default receiver position ([0 0 0]) removed. Receiver becomes active after position is manually set.
  - CTC crashed due to this change, fixed now.
- Timer in VAMatlab had performance issues after Windows update: Now uses highest possible clockrate and should have same performance as in initial implementation
- CTC crashed when the HRTF did not cover all directions; fixed in ITACoreLibs/ITACTC.
- Prototype Room Acoustic Renderer:
  - Convolution was only mono.
  - Rotation was flipped.
  - Only the first Source-Receiver-Pair received RIR updates due to copy past error.
- ITASimulationScheduler: Source-Receiver-Pair balancing

- VAMatlab: Empty VAStructs caused crash due to out of bounds vector access.
  This probably followed due to stricter exceptions in C++17.

-----------
## [v2021a] - 2021-03-18

### Changed

#### General

- set active listener method was removed

#### BinauralOutdoorNoiseRenderer

- now works for atmospheric and urban propagation path simulations (Atmospheric Ray Tracing & Image Edge Model)
- Beta: Now is able to schedule simulations (for future real-time applications)

#### AirtraficNoiseRenderer

- now has option to use homogeneous or inhomogeneous medium
- for inhomogeneous medium, the Atmospheric Ray Tracing (ART) framework is used for simulation of sound paths
- for homogeneous medium, straight paths are assumed

#### VAServer / VACore

- now has a new ini file for offline rendering storing the result in a wave-file
  - see run_VAServer_recording.bat & VACore.recording.ini

#### VAPython

- Python bindings now work with Python version 3.7 to 3.9
